# LRTP Input Map

Interactive input map for the [Long Range Transportation
Plan](https://lrtp.cuuats.org/) using [Web Map
GL](https://gitlab.com/ccrpc/webmapgl).

## Building

This project is hosted via gitlab pages, the CI/CD is set up to build those pages whenever something is pushed to either the master branch or the create-pages branch.

## License

LRTP Input Map is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/transportation-voices-2050/blob/master/LICENSE.md).
