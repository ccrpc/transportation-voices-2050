# lrtp-survey-controller



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description | Type     | Default |
| ------------------ | ------------------- | ----------- | -------- | ------- |
| `commentThreshold` | `comment-threshold` |             | `number` | `1`     |
| `delay`            | `delay`             |             | `number` | `3000`  |
| `likeThreshold`    | `like-threshold`    |             | `number` | `2`     |


## Methods

### `create() => Promise<HTMLIonAlertElement>`



#### Returns

Type: `Promise<HTMLIonAlertElement>`




----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
