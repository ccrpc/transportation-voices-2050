# lrtp-comment-add



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type     | Default     |
| -------------- | --------------- | ----------- | -------- | ----------- |
| `label`        | `label`         |             | `string` | `undefined` |
| `schema`       | `schema`        |             | `string` | `undefined` |
| `token`        | `token`         |             | `string` | `undefined` |
| `toolbarLabel` | `toolbar-label` |             | `string` | `undefined` |
| `url`          | `url`           |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
