# lrtp-address-search



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type                               | Default         |
| ------------------- | --------------------- | ----------- | ---------------------------------- | --------------- |
| `bbox`              | --                    |             | `[number, number, number, number]` | `undefined`     |
| `forwardGeocodeUrl` | `forward-geocode-url` |             | `string`                           | `undefined`     |
| `jobId`             | `job-id`              |             | `string`                           | `'lrtp-search'` |
| `limit`             | `limit`               |             | `number`                           | `10`            |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
