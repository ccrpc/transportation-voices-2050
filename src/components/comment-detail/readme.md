# lrtp-comment-detail



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description | Type      | Default     |
| ------------ | ------------- | ----------- | --------- | ----------- |
| `allowLike`  | `allow-like`  |             | `boolean` | `true`      |
| `feature`    | `feature`     |             | `any`     | `undefined` |
| `features`   | --            |             | `any[]`   | `undefined` |
| `likeUrl`    | `like-url`    |             | `string`  | `undefined` |
| `popup`      | `popup`       |             | `boolean` | `false`     |
| `selectedId` | `selected-id` |             | `number`  | `undefined` |
| `token`      | `token`       |             | `string`  | `undefined` |


## Events

| Event                  | Description | Detail |
| ---------------------- | ----------- | ------ |
| `commentSelectedEvent` |             | any    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
