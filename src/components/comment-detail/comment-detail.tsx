import { Component, Prop, Event, EventEmitter } from "@stencil/core";
import { _t } from "../i18n/i18n";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

@Component({
  styleUrl: "comment-detail.css",
  tag: "lrtp-comment-detail",
})
export class CommentDetail {
  @Prop() allowLike: boolean = true;
  @Prop() likeUrl: string;
  @Prop({ mutable: true }) feature: any;
  @Prop() features: any[];
  @Prop() token: string;
  @Prop() popup: boolean = false;
  @Prop() selectedId: number;

  @Event() commentSelectedEvent: EventEmitter<any>;

  componentWillLoad() {
    if (this.features && !this.feature) this.feature = this.features[0];
    if (this.feature.properties.comment_description === "null")
      delete this.feature.properties.comment_description;
  }

  doLocate() {
    if (!this.feature.geometry) return;
    let mapEl = document.querySelector("gl-map");
    mapEl.map.flyTo({
      center: this.feature.geometry.coordinates,
      zoom: 18,
    });
  }

  hostData() {
    return {
      class: `lrtp-mode-${this.feature.properties.comment_mode}`,
    };
  }

  render() {
    const small = screen.width <= 640;
    const props = this.feature.properties;
    const mode = props.comment_mode;
    const modeImage = "/transportation-voices-2050/public/img/" + mode + ".png";
    const comment = (
      _t(`lrtp.form.${mode}.label`) +
      ": " +
      _t(`lrtp.form.${props.comment_type}`)
    ).replace(/\([^)]*\)/g, "");

    let dateStr = "";
    let date = new Date(this.feature.properties._created);
    if (!isNaN(date.getTime())) {
      dateStr =
        `${monthNames[date.getMonth()]} ${date.getDate()}, ` +
        `${date.getFullYear()}`;
      if (props.comment_description) dateStr += ":";
    }

    let content = [];
    if (!small)
      content.push(
        <ion-avatar slot="start">
          <img src={modeImage} alt={mode} />
        </ion-avatar>
      );

    content.push(
      <ion-label text-wrap>
        <h2>{comment}</h2>
        <p>
          <strong>{dateStr}</strong> {props.comment_description || null}
        </p>
      </ion-label>
    );

    if (!this.popup) {
      content.push(
        <ion-button
          fill="clear"
          slot="end"
          class="lrtp-locate-button"
          onClick={() => this.doLocate()}
        >
          <ion-icon slot="icon-only" name="locate"></ion-icon>
        </ion-button>
      );

      content.push(
        <gl-like-button
          slot="end"
          url={this.likeUrl}
          token={this.token}
          feature={this.feature}
          disabled={!this.allowLike}
        ></gl-like-button>
      );
    }

    return (
      <ion-item
        lines={this.popup ? "none" : "full"}
        color={
          this.selectedId && this.selectedId === this.feature.id
            ? "secondary"
            : ""
        }
        id={
          this.selectedId && this.selectedId === this.feature.id
            ? "selectedComment"
            : ""
        }
        onClick={() => {
          if (this.popup) {
            this.commentSelectedEvent.emit(this.feature);
          }
        }}
      >
        {content}
      </ion-item>
    );
  }
}
