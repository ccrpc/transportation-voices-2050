# lrtp-app



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type      | Default     |
| ------------------- | --------------------- | ----------- | --------- | ----------- |
| `allowInput`        | `allow-input`         |             | `boolean` | `true`      |
| `bbox`              | `bbox`                |             | `string`  | `undefined` |
| `commentUrl`        | `comment-url`         |             | `string`  | `undefined` |
| `forwardGeocodeUrl` | `forward-geocode-url` |             | `string`  | `undefined` |
| `likeUrl`           | `like-url`            |             | `string`  | `undefined` |
| `multiuser`         | `multiuser`           |             | `boolean` | `false`     |
| `schemaUrl`         | `schema-url`          |             | `string`  | `undefined` |
| `shouldShowIntro`   | `should-show-intro`   |             | `boolean` | `false`     |
| `styleUrl`          | `style-url`           |             | `string`  | `undefined` |
| `surveyUrl`         | `survey-url`          |             | `string`  | `undefined` |
| `token`             | `token`               |             | `string`  | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
