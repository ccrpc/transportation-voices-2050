/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import '@stencil/core';

import '@ionic/core';
import 'ionicons';
import '@ccrpc/webmapgl';


export namespace Components {

  interface LrtpAddressSearch {
    'bbox': [number, number, number, number];
    'forwardGeocodeUrl': string;
    'jobId': string;
    'limit': number;
  }
  interface LrtpAddressSearchAttributes extends StencilHTMLAttributes {
    'bbox'?: [number, number, number, number];
    'forwardGeocodeUrl'?: string;
    'jobId'?: string;
    'limit'?: number;
  }

  interface LrtpApp {
    'allowInput': boolean;
    'bbox': string;
    'commentUrl': string;
    'forwardGeocodeUrl': string;
    'likeUrl': string;
    'multiuser': boolean;
    'schemaUrl': string;
    'shouldShowIntro': boolean;
    'styleUrl': string;
    'surveyUrl': string;
    'token': string;
  }
  interface LrtpAppAttributes extends StencilHTMLAttributes {
    'allowInput'?: boolean;
    'bbox'?: string;
    'commentUrl'?: string;
    'forwardGeocodeUrl'?: string;
    'likeUrl'?: string;
    'multiuser'?: boolean;
    'schemaUrl'?: string;
    'shouldShowIntro'?: boolean;
    'styleUrl'?: string;
    'surveyUrl'?: string;
    'token'?: string;
  }

  interface LrtpCommentAdd {
    'label': string;
    'schema': string;
    'token': string;
    'toolbarLabel': string;
    'url': string;
  }
  interface LrtpCommentAddAttributes extends StencilHTMLAttributes {
    'label'?: string;
    'schema'?: string;
    'token'?: string;
    'toolbarLabel'?: string;
    'url'?: string;
  }

  interface LrtpCommentDetail {
    'allowLike': boolean;
    'feature': any;
    'features': any[];
    'likeUrl': string;
    'popup': boolean;
    'selectedId': number;
    'token': string;
  }
  interface LrtpCommentDetailAttributes extends StencilHTMLAttributes {
    'allowLike'?: boolean;
    'feature'?: any;
    'features'?: any[];
    'likeUrl'?: string;
    'onCommentSelectedEvent'?: (event: CustomEvent<any>) => void;
    'popup'?: boolean;
    'selectedId'?: number;
    'token'?: string;
  }

  interface LrtpSurveyController {
    'commentThreshold': number;
    'create': () => Promise<HTMLIonAlertElement>;
    'delay': number;
    'likeThreshold': number;
  }
  interface LrtpSurveyControllerAttributes extends StencilHTMLAttributes {
    'commentThreshold'?: number;
    'delay'?: number;
    'likeThreshold'?: number;
  }
}

declare global {
  interface StencilElementInterfaces {
    'LrtpAddressSearch': Components.LrtpAddressSearch;
    'LrtpApp': Components.LrtpApp;
    'LrtpCommentAdd': Components.LrtpCommentAdd;
    'LrtpCommentDetail': Components.LrtpCommentDetail;
    'LrtpSurveyController': Components.LrtpSurveyController;
  }

  interface StencilIntrinsicElements {
    'lrtp-address-search': Components.LrtpAddressSearchAttributes;
    'lrtp-app': Components.LrtpAppAttributes;
    'lrtp-comment-add': Components.LrtpCommentAddAttributes;
    'lrtp-comment-detail': Components.LrtpCommentDetailAttributes;
    'lrtp-survey-controller': Components.LrtpSurveyControllerAttributes;
  }


  interface HTMLLrtpAddressSearchElement extends Components.LrtpAddressSearch, HTMLStencilElement {}
  var HTMLLrtpAddressSearchElement: {
    prototype: HTMLLrtpAddressSearchElement;
    new (): HTMLLrtpAddressSearchElement;
  };

  interface HTMLLrtpAppElement extends Components.LrtpApp, HTMLStencilElement {}
  var HTMLLrtpAppElement: {
    prototype: HTMLLrtpAppElement;
    new (): HTMLLrtpAppElement;
  };

  interface HTMLLrtpCommentAddElement extends Components.LrtpCommentAdd, HTMLStencilElement {}
  var HTMLLrtpCommentAddElement: {
    prototype: HTMLLrtpCommentAddElement;
    new (): HTMLLrtpCommentAddElement;
  };

  interface HTMLLrtpCommentDetailElement extends Components.LrtpCommentDetail, HTMLStencilElement {}
  var HTMLLrtpCommentDetailElement: {
    prototype: HTMLLrtpCommentDetailElement;
    new (): HTMLLrtpCommentDetailElement;
  };

  interface HTMLLrtpSurveyControllerElement extends Components.LrtpSurveyController, HTMLStencilElement {}
  var HTMLLrtpSurveyControllerElement: {
    prototype: HTMLLrtpSurveyControllerElement;
    new (): HTMLLrtpSurveyControllerElement;
  };

  interface HTMLElementTagNameMap {
    'lrtp-address-search': HTMLLrtpAddressSearchElement
    'lrtp-app': HTMLLrtpAppElement
    'lrtp-comment-add': HTMLLrtpCommentAddElement
    'lrtp-comment-detail': HTMLLrtpCommentDetailElement
    'lrtp-survey-controller': HTMLLrtpSurveyControllerElement
  }

  interface ElementTagNameMap {
    'lrtp-address-search': HTMLLrtpAddressSearchElement;
    'lrtp-app': HTMLLrtpAppElement;
    'lrtp-comment-add': HTMLLrtpCommentAddElement;
    'lrtp-comment-detail': HTMLLrtpCommentDetailElement;
    'lrtp-survey-controller': HTMLLrtpSurveyControllerElement;
  }


  export namespace JSX {
    export interface Element {}
    export interface IntrinsicElements extends StencilIntrinsicElements {
      [tagName: string]: any;
    }
  }
  export interface HTMLAttributes extends StencilHTMLAttributes {}

}
