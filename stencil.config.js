exports.config = {
  namespace: "lrtp",
  copy: [
    {
      src: "public",
    },
  ],
  outputTargets: [
    {
      type: "www",
      baseUrl: "/transportation-voices-2050",
      serviceWorker: null,
    },
  ],
  plugins: [],
  globalScript: "src/global/lrtp.ts",
  globalStyle: "src/global/lrtp.css",
};
